
var Signal = require('../schlomix-node').Signal;

exports['Test Signal'] = function (test) {
    test.expect(6);

    var signal = new Signal();

    var connectedSignal = signal.connect(function (a, b, c) {
        test.equal(a, 1);
        test.equal(b, 2);
        test.equal(c, 3);
        test.equal(arguments.length, 3);
    });

    test.strictEqual(connectedSignal, signal, 'The object returned from connect() should be the Signal');

    process.nextTick(function () {
        var emittedSignal = signal.emit(1,2,3);
        test.strictEqual(emittedSignal, signal, 'The object returned from emit() should be the Signal');

        test.done();
    })
}
