
var Future = require('../schlomix-node').Future;

exports['Test Accept/Success'] = function (test) {

    var future = new Future();

    future.onSuccess(function (a, b, c) {
        test.equal(a, 1);
        test.equal(b, 2);
        test.equal(c, 3);
        test.done();
    });

    process.nextTick(function () {
        future.accept(1,2,3);
    });
};

exports['Test Accept/Success Callback Scope'] = function (test) {

    var future = new Future();

    var myScope = {
        key: 'value'
    };

    future.onSuccess(function () {
        test.equal(this.key, 'value');
        test.strictEqual(this, myScope);
        test.done();
    }, myScope);

    process.nextTick(function () {
        future.accept();
    });
};

exports['Test Reject/Fail'] = function (test) {
    var future = new Future();

    future.onFail(function (err) {

        test.ok(err instanceof Error, 'err should be an instance of Error');
        test.equal(err.message, 'oh no!');
        test.done();
    });

    process.nextTick(function () {
        future.reject(new Error('oh no!'));
    });
};

exports['Test Reject/Fail Callback Scope'] = function (test) {
    var future = new Future();

    var myScope = {
        key: 'value'
    };

    future.onFail(function (err) {
        test.equal(this.key, 'value');
        test.strictEqual(this, myScope);
        test.done();
    }, myScope);

    process.nextTick(function () {
        future.reject('ERROR!');
    });
};

exports['Test Finally (With Success)'] = function (test) {
    var future = new Future();

    var d1 = 0,
        d2 = 0;

    test.expect(2);

    future.onSuccess(function () {
        d1 = 1;
    }).onFinally(function () {
        d2 = d1 + 1;

        test.ok(d2 > d1, 'd2 should be greater than d1 -- finally should execute AFTER success');
        test.equal(d2, 2);
        test.done();
    })

    process.nextTick(function () {
        future.accept();
    });
};

exports['Test Finally (With Fail)'] = function (test) {
    var future = new Future();

    var d1 = 0,
        d2 = 0;

    test.expect(2);

    future.onFail(function (err) {
        d1 = 1;
    }).onFinally(function () {
        d2 = d1 + 1;

        test.ok(d2 > d1, 'd2 should be greater than d1 -- finally should execute AFTER fail');
        test.equal(d2, 2);
        test.done();
    })

    process.nextTick(function () {
        future.reject('ERROR!');
    });
};

exports['Test Finally Callback Scope'] = function (test) {
    var future = new Future();

    var myScope = {
        key: 'value'
    };

    future.onFinally(function () {

        test.equal(this.key, 'value');
        test.strictEqual(this, myScope);
        test.done();

    }, myScope);

    process.nextTick(function () {
        future.accept();
    });
};
