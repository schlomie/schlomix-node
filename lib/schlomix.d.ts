
interface Slot {
    method: Function;
    scope?: Object;
}
