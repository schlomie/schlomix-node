var future = require('./future');
var signal = require('./signal');
exports.Signal = signal.Signal;
exports.Future = future.Future;
