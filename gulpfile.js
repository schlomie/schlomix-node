
var gulp = require('gulp'),
    tsc = require('gulp-tsc');

var paths = {
    ts:     'src/**/*.ts',
    dest:   './'
};

gulp.task('compile', function () {
    gulp.src([paths.ts])
        .pipe(tsc({
            module:             'commonjs',
            target:             'ES5',
            removeComments:     true
        }))
        .pipe(gulp.dest(paths.dest));
});

gulp.task('default', ['compile']);
