var Signal = (function () {
    function Signal() {
        this._listeners = [];
    }
    Signal.prototype.removeAllListeners = function () {
        this._listeners = [];
        return this;
    };
    Signal.prototype.connect = function (method, scope) {
        this._listeners.push({ method: method, scope: scope });
        return this;
    };
    Signal.prototype.emit = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        this._listeners.forEach(function (listener) {
            listener.method.apply(listener.scope, args);
        });
        return this;
    };
    return Signal;
})();
exports.Signal = Signal;
