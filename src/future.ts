
/// <reference path="../lib/schlomix.d.ts" />

export class Future {

    private _successListeners: Slot[] = [];
    private _failListeners: Slot[] = [];
    private _finallyListeners: Slot[] = [];
    private _disposed: boolean = false;

    private _bind (slots: Slot[], method: Function, scope?: Object): Future {
        slots.push({method:method,scope:scope});
        return this;
    }

    private _executeListeners (slots: Slot[], args: any[]): Future {
        slots.forEach((slot: Slot) => {
            slot.method.apply(slot.scope, args);
        });

        return this;
    }

    private _fire (err?: any, ...args: any[]): Future {
        if (err) {
            this._executeListeners(this._failListeners, [err]);
        } else {
            this._executeListeners(this._successListeners, args);
        }

        return this._executeListeners(this._finallyListeners, []).dispose();
    }

    public onSuccess(method: Function, scope?: Object): Future {
        return this._bind(this._successListeners, method, scope);
    }

    public onFail(method: Function, scope?: Object): Future {
        return this._bind(this._failListeners, method, scope);
    }

    public onFinally(method: Function, scope?: Object): Future {
        return this._bind(this._finallyListeners, method, scope);
    }

    public removeAllListeners (): Future {
        this._successListeners = [];
        this._failListeners = [];
        this._finallyListeners = [];
        return this;
    }

    public reset (): Future {
        this._disposed = false;
        return this.removeAllListeners();
    }

    public dispose (): Future {
        this._disposed = true;
        return this.removeAllListeners();
    }

    public isDisposed (): boolean {
        return this._disposed;
    }

    public accept (...args: any[]): Future {
        args.unshift(null);
        return this._fire.apply(this, args);
    }

    public reject (err: Error): Future {
        return this._fire(err);
    }
}
