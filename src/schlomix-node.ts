
import future = require('./future');
import signal = require('./signal');

export var Signal = signal.Signal;
export var Future = future.Future;
