/// <reference path="../lib/schlomix.d.ts" />

export class Signal {

    private _listeners: Slot[] = [];

    public removeAllListeners(): Signal {
        this._listeners = [];
        return this;
    }

    public connect (method: Function, scope?: Object): Signal {
        this._listeners.push({method:method,scope:scope});
        return this;
    }

    public emit (...args: any[]): Signal {
        this._listeners.forEach(function (listener) {
            listener.method.apply(listener.scope, args);
        });

        return this;
    }
}
