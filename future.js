var Future = (function () {
    function Future() {
        this._successListeners = [];
        this._failListeners = [];
        this._finallyListeners = [];
        this._disposed = false;
    }
    Future.prototype._bind = function (slots, method, scope) {
        slots.push({ method: method, scope: scope });
        return this;
    };
    Future.prototype._executeListeners = function (slots, args) {
        slots.forEach(function (slot) {
            slot.method.apply(slot.scope, args);
        });
        return this;
    };
    Future.prototype._fire = function (err) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (err) {
            this._executeListeners(this._failListeners, [err]);
        }
        else {
            this._executeListeners(this._successListeners, args);
        }
        return this._executeListeners(this._finallyListeners, []).dispose();
    };
    Future.prototype.onSuccess = function (method, scope) {
        return this._bind(this._successListeners, method, scope);
    };
    Future.prototype.onFail = function (method, scope) {
        return this._bind(this._failListeners, method, scope);
    };
    Future.prototype.onFinally = function (method, scope) {
        return this._bind(this._finallyListeners, method, scope);
    };
    Future.prototype.removeAllListeners = function () {
        this._successListeners = [];
        this._failListeners = [];
        this._finallyListeners = [];
        return this;
    };
    Future.prototype.reset = function () {
        this._disposed = false;
        return this.removeAllListeners();
    };
    Future.prototype.dispose = function () {
        this._disposed = true;
        return this.removeAllListeners();
    };
    Future.prototype.isDisposed = function () {
        return this._disposed;
    };
    Future.prototype.accept = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        args.unshift(null);
        return this._fire.apply(this, args);
    };
    Future.prototype.reject = function (err) {
        return this._fire(err);
    };
    return Future;
})();
exports.Future = Future;
